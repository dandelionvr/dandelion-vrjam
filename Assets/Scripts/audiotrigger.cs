﻿using UnityEngine;
using System.Collections;

public class audiotrigger : MonoBehaviour {

	public AudioClip[] SoundFX;

	private AudioSource source = null;

	// Use this for initialization
	void Start () 
	{
		source = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter()
	{
		if (source != null) {
			source.pitch = Random.Range (.8f, 1.2f);
			source.volume = Random.Range (.1f, .2f);
			source.PlayOneShot (SoundFX [Random.Range (0, SoundFX.Length)]);
		}
	}
}
