﻿using UnityEngine;
using System.Collections;

public class AnimateScript : MonoBehaviour {

	public Animator characterAnim;

	private OVRPlayerControllerFloat playerController = null;
	private OVRPlayerControllerFloat.movementStatuses movementStatus = OVRPlayerControllerFloat.movementStatuses.idle;

	// Use this for initialization
	void Start () 
	{
		playerController = GetComponentInParent<OVRPlayerControllerFloat> ();

		characterAnim.SetBool("isWalking", false);
		characterAnim.SetBool("isRunning", false);
		characterAnim.SetBool("isJumping", false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		movementStatus = playerController.GetMovementStatus ();
		switch (movementStatus) {
		case OVRPlayerControllerFloat.movementStatuses.walking:
			Walk();
			break;
		case OVRPlayerControllerFloat.movementStatuses.running:
			Run();
			break;
		case OVRPlayerControllerFloat.movementStatuses.jumping:
			Jump();
			break;
		case OVRPlayerControllerFloat.movementStatuses.idle:
			Stop();
			break;
		}
	}
	
	void Stop()
	{
		characterAnim.SetBool ("isIdle", true);
		characterAnim.SetBool ("isWalking", false);
		characterAnim.SetBool ("isRunning", false);
		characterAnim.SetBool ("isJumping", false);
	}

	void Walk()
	{
		if (movementStatus == OVRPlayerControllerFloat.movementStatuses.walking)
		{		
			characterAnim.SetBool ("isIdle", false);
			characterAnim.SetBool ("isRunning", false);
			characterAnim.SetBool ("isJumping", false);
			characterAnim.SetBool("isWalking", true);

		}
	}
	void Run()
	{
		if (movementStatus == OVRPlayerControllerFloat.movementStatuses.running)
		{		
			characterAnim.SetBool ("isIdle", false);
			characterAnim.SetBool ("isJumping", false);
			characterAnim.SetBool("isWalking", false);
			characterAnim.SetBool ("isRunning", true);
		}
	}
	void Jump()
	{
		if (movementStatus == OVRPlayerControllerFloat.movementStatuses.jumping)
		{		
			characterAnim.SetBool ("isIdle", false);
			characterAnim.SetBool("isWalking", false);
			characterAnim.SetBool ("isRunning", false);
			characterAnim.SetBool ("isJumping", true);
		}
	}
}
