﻿using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour {

	public GameObject player = null;
	public ParticleSystem highlights = null;
	public float normalEmissionRate = 200f;
	public float highlightedEmissionRate = 1000f;
	
	protected Color color = Color.white;
	protected bool highlighted = false;
	protected OVRPlayerControllerFloat playerController = null;
	protected DVRInteraction playerInteraction = null;
	protected bool isCarried = false;

	// Use this for initialization
	protected virtual void Start () {
		if (player != null) {
			playerController = player.GetComponent<OVRPlayerControllerFloat> ();
			playerInteraction = player.GetComponent<DVRInteraction>();
		}
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if (highlights != null) {
			if (isCarried) {
				highlights.enableEmission = false;
			} else {

				if (highlighted) {
					highlights.emissionRate = highlightedEmissionRate;
					highlighted = false;
				} else {
					highlights.emissionRate = normalEmissionRate;
				}
				highlights.enableEmission = true;
			}
		}
	}
	
	public void Highlight() {
		highlighted = true;
	}
	
	public virtual void Activate() {
		// default Activation is to pick up item, but child can override
		if (!isCarried && playerInteraction != null) {
			playerInteraction.PickUp(gameObject);
		}
	}

	public virtual void SetCarried(bool carried) {
		isCarried = carried;
	}
}
