﻿using UnityEngine;
using System.Collections;

public class Whirlwind : InteractiveObject {
	
	public float enterOffset = 0.01f;
	public float whirlwindCeiling = 1.5f;
	public float speed = 0.1f;
	public GameObject dandelionFull;
	public GameObject dandelionStem;
	public ParticleSystem highlightStem;
	public AudioClip[] whirlwindSounds;
	
	private AudioSource source = null;
	private bool isMovingUp = false;
	private Vector3 target = Vector3.zero;
	private ParticleSpin spinner = null;
	private float spinFactor = 0.5f;
	private PlayerSound playerSound = null;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		source = GetComponent<AudioSource> ();
		spinner = GetComponentInChildren<ParticleSpin> ();
		if (spinner != null) {
			// Player should spin more slowly than the seeds
			spinFactor = (float)spinner.spinFactor * 0.75f;
		}
	}
	
	// Update is called once per frame
	protected override void Update () {
		base.Update ();

		if (isMovingUp) {
			float step = speed * Time.deltaTime;
			player.transform.position = Vector3.MoveTowards (player.transform.position, target, step);
			player.transform.Rotate (Vector3.up * spinFactor);
			if (Vector3.Distance (player.transform.position, target) < 0.01f) {
				ExitWhirlwind();
			}
		}
	}

	public override void Activate() {
		if (dandelionFull != null && dandelionStem != null) {
			dandelionStem.SetActive(true);
			dandelionFull.SetActive(false);
			highlights = highlightStem;
		}
		if (playerController != null) {
			playerController.Jump ();
		}
	}

	public void EnterWhirlwind() {
		target = new Vector3 (transform.position.x, whirlwindCeiling, transform.position.z);
		if (Vector3.Distance (player.transform.position, target) > 0.2f && playerController != null) {
			playerController.SetGravity (false);
			playerController.SetHaltUpdateMovement(true);
			isMovingUp = true;
			WhirlwindSound();
		}
	}

	public void RideWhirlwind() {
		if (Vector3.Distance (player.transform.position, target) > 0.2f) {
			if (playerController != null) {
				playerController.SetGravity (false);
				isMovingUp = true;
			}
		}
	}

	public void ExitWhirlwind() {
		isMovingUp = false;
		playerController.SetGravity (true);
		playerController.SetHaltUpdateMovement(false);
		playerController.CauseUpdraft ();
	}

	private void WhirlwindSound() {
		if (source != null) {
			source.pitch = Random.Range (.8f,1.2f);
			source.volume = Random.Range (.1f,.2f);
			source.PlayOneShot (whirlwindSounds [Random.Range (0,whirlwindSounds.Length)]);
		}
	}

}
