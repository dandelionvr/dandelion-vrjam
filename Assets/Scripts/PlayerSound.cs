﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSound : MonoBehaviour 
{
	//Movement Audio
	public AudioClip[] walkGrass;
	public AudioClip[] walkGravel;
	public AudioClip[] walkSand;
	public AudioClip[] walkStone;
	public AudioClip[] walkPlastic;
	public AudioClip[] walkWood;
	public AudioClip[] jumpWoosh;
	public AudioClip[] whirlwind;

	//Idle Audio
	public AudioClip[] idleSounds;

	private AudioSource source = null;
	
	void Start () 
	{
		source = GetComponent<AudioSource> ();
	}
	
	private void IdleSound()
	{
		if (source != null) {
			source.clip = idleSounds [Random.Range (0, idleSounds.Length)];
			source.pitch = Random.Range (.9f, 1.1f);
			source.volume = Random.Range (.3f, .4f);
			source.Play ();
		}
	}

	private void WalkSound()
	{
		if (source != null) {
			source.clip = getClip ();
			source.pitch = Random.Range (1f, 1.25f);
			source.volume = Random.Range (.3f, .4f);
			source.Play ();
		}
	}

	private void RunSound()
	{
		if (source != null) {
			source.clip = getClip ();
			source.pitch = Random.Range (1.1f, 1.4f);
			source.volume = Random.Range (.3f, .4f);
			source.Play ();
		}
	}

	private void JumpUpWoosh()
	{
		if (source != null) {
			source.pitch = Random.Range (1f, 1.1f);
			source.volume = Random.Range (.5f, .8f);
			source.PlayOneShot (jumpWoosh [Random.Range (0, jumpWoosh.Length)]);
		}
	}

	private void JumpDownSound()
	{
		if (source != null) {
			source.clip = getClip ();
			source.pitch = Random.Range (1.1f, 1.4f);
			source.volume = Random.Range (.1f, .2f);
			source.Play ();
		}
	}

	private AudioClip getClip() {
		string surface = "";
		// grass is default surface
		AudioClip[] clipArray = walkGrass;
		RaycastHit hit;

		if (Physics.Raycast (transform.position, Vector3.down, out hit)) {
			surface = hit.transform.gameObject.tag;
		}


		switch (surface) {
		case "Gravel":
			clipArray = walkGravel;
			break;
		case "Plastic":
			clipArray = walkPlastic;
			break;
		case "Sand":
			clipArray = walkSand;
			break;
		case "Stone":
			clipArray = walkStone;
			break;
		case "Wood":
			clipArray = walkWood;
			break;
		}

		int max = clipArray.Length;
		return clipArray [Random.Range (0, max)];
	}
}
