﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour {

	public Image progress;
	private AsyncOperation async;

	// Use this for initialization
	IEnumerator Start() {
		progress.fillAmount = 0;
		yield return new WaitForSeconds (1);
		async = Application.LoadLevelAsync("Main");
		yield return async;
	}

	void FixedUpdate() {
		if (async != null) {
			progress.fillAmount = async.progress;
		}
	}
}
