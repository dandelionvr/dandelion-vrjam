﻿using UnityEngine;
using System.Collections;

public class ParticleSpin : MonoBehaviour {

	public int spinFactor = 5;

	private Whirlwind whirlwind = null;

	void Start() {
		whirlwind = GetComponentInParent<Whirlwind> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.Rotate (Vector3.forward * spinFactor);
	}

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player" && whirlwind != null) {
			whirlwind.EnterWhirlwind();
		}
	}

	/*
	 * @TODO: Causes player to become stuck at the top
	public void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == "Player" && whirlwind != null) {
			whirlwind.RideWhirlwind();
		}
	}


	public void OnTriggerExit(Collider other) {
		if (other.gameObject.tag == "Player" && whirlwind != null) {
			whirlwind.ExitWhirlwind();
		}
	}
	*/
}

