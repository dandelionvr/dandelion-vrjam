﻿using UnityEngine;
using System.Collections;

public class DVRInteraction : MonoBehaviour {

	public float interactionDistance = 0.2f;

	private OVRPlayerControllerFloat playerController = null;
	private Transform centerEye = null;
	private GameObject carriedItem = null;
	private bool isFirstClick = true;

	private FixedJoint fj = null;
	private Rigidbody carriedItemRb = null;
	private float carriedItemMass = 0f;
	private InteractiveObject carriedItemScript = null;

	// Use this for initialization
	void Start () {
		playerController = GetComponent<OVRPlayerControllerFloat> ();
		if (playerController == null) {
			Debug.LogWarning ("No OVRPlayerControllerFloat");
		} else {
			OVRCameraRig cameraRig = playerController.GetCameraRig();
			centerEye = cameraRig.centerEyeAnchor;
			playerController.SetMoveScaleMultiplier(2.0f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		bool activateButtonPressed = false;

		// did player hit the activate button?
		if (OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.B) || Input.GetButton ("Fire1") || Input.touchCount > 0) {
			// have to check whether this is the first click since OVRGamePad doesn't have a GetButtonDown method
			if (isFirstClick) {
				activateButtonPressed = true;
				isFirstClick = false;
			}
		} else {
			isFirstClick = true;
		}

		if (activateButtonPressed && carriedItem != null) {
			Drop ();
		} else if (Physics.Raycast(centerEye.position, centerEye.forward, out hit, interactionDistance)) {
			InteractiveObject interactiveObject = hit.transform.gameObject.GetComponent<InteractiveObject>();
			if (interactiveObject != null && hit.transform.gameObject != carriedItem) {
				interactiveObject.Highlight();
				if (activateButtonPressed) {
					interactiveObject.Activate();
				}
			}
		}
	}

	public void PickUp(GameObject item) {
		carriedItem = item;
		carriedItemRb = carriedItem.GetComponent<Rigidbody> ();
		carriedItemRb.useGravity = false;
		carriedItemMass = carriedItemRb.mass;
		carriedItemRb.mass = 0;
		fj = centerEye.gameObject.AddComponent<FixedJoint> ();
		fj.connectedBody = carriedItemRb;
		carriedItemScript = carriedItem.GetComponent<InteractiveObject> ();
		carriedItemScript.SetCarried (true);
	}

	public void Drop() {
		if (carriedItem != null) {
			fj.connectedBody = null;
			carriedItemRb.useGravity = true;
			carriedItemRb.mass = carriedItemMass;
			Destroy(fj);
			carriedItemScript.SetCarried(false);
			carriedItem = null;
		}
	}
}
